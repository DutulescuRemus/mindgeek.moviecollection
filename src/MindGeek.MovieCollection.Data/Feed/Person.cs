﻿namespace MindGeek.MovieCollection.Data.Feed
{
    /// <summary>
    /// Person (e.g.: actor, director, etc)
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Person name
        /// </summary>
        public string Name { get; set; }
    }

}

import { Image } from './image';
import { Person } from './person';
import { Video } from './video';
import { MovieViewingWindow } from './movie-viewing-window';

export class Movie {
  id: string;
  body: string;
  cardImages: Image[];
  cast: Person[];
  cert: string;
  class: string;
  directors: Person[];
  duration?: number;
  genres: string[];
  headline: string;
  keyArtImages: Image[];
  lastUpdated?: Date;
  quote: string;
  rating?: number;
  reviewAuthor: string;
  skyGoId: string;
  skyGoUrl: string;
  sum: string;
  url: string;
  videos: Video[];
  viewingWindow: MovieViewingWindow;
  year: number;
}

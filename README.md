# MindGeek.MovieCollection - Interview Project #

## REQUIREMENT ##

You will need to write a program that downloads all the items in https://mgtechtest.blob.core.windows.net/files/showcase.json  and cache images within each asset. To make it efficient, it is desired to only call the URLs in the JSON file only once. Demonstrate, by using a framework of your choice, your software architectural skills. How you use the framework will be highly important in the evaluation. 

How you display the feed and how many layers/pages you use is up to you, but please ensure that we can see the complete list and the details of every item. You will likely hit some road blocks and errors along the way, please use your own initiative to deal with these issues, it’s part of the test.

Please ensure all code is tested before sending it back, it would be good to also see unit tests too. Ideally, alongside supplying the code base and all packages/libraries required to deploy, you will also have to supply deployment instructions too.


## PROJECT OVERVIEW ##

### Architecture diagram ###
![picture](util/c4-diagram.png)

### Frameworks: ###
* Backend: **.NET Core 3.1**
* Froentend client: **Angular 9**
* API Documentation: **Swagger**
* Logging: **Serilog** with **Console and AzureBlob sinks** 
* Unit testing: **xUnit, Moq**
* Integration testing: **Postman**
* **Azure infrastructure:** 
    * **App Service** - API project `MindGeek.MovieCollection.Api`
    * **Redis Cache** - caching processed JSON feed
    * **Azure Blob Storage** - image persisting, log storage
    * **Azure KeyVault** - secret management
    * **Application Insights** - availability test for API endpoint
* Heroku infrastructure - Client application deployment

### Approach notes ###
* The JSON feed processing is done using a dedicated [hosted service]('https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-3.1&tabs=visual-studio') that does the following:
    1. Get JSON data and parse it;
    2. Check each image link if it was previously persisted. Persist it to **Azure Blob storage** it not, then update the URL;
    3. Remove `Image` entry if the link is dead and it cannot be found on the persistence layer; 
    3. Set the processed JSON cache (`Redis`), to be read by the API service;
* The API service then:
    1. Checks if the cache is set on Redis and, if so, deserialize and serve it;
    2. (fallback) Trigger the processing logic again if the cache is not found, strengthening resilience.
* The cached and processed JSON has a configurable `timeToLive` and expires after it;
* The worker service uses a CRON to trigger itself every 10 minutes (which is less than the cache time-to-live), so that it can keep the cache fresh (**note:** the general assumption was that the feed may serve different data across time, therefore the approach was to check for differences periodically).
* The observed anomaly character is removed during parsing.
* The system, overall, was designed with high resilience and scalability in mind. Therefore, The API service can scale (individually from the worker) if the scenario load was to require it.

## BUILD | DEPLOYMENT | TEST ##

### Build requirements ###
* [.NET Core SDK 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1)
* [Visual Studio 2019 Community](https://visualstudio.microsoft.com/vs/) (or any other IDE of your choice)
* [NodeJS LTS 12.16.2](https://nodejs.org/en/download/)
* [Angular CLI](https://cli.angular.io/) `npm install -g @angular/cli`
* (optional) [Postman](https://www.postman.com/)


### Cloud ###

The project has already been fully deployed on Azure. The client application has been deployed to Heroku. 

 1. **Client deployment** - https://mindgeek-mc-client.herokuapp.com
 2. **API deployment** - https://mindgeek-mc-api.azurewebsites.net
 3. **API Swagger UI** - https://mindgeek-mc-api.azurewebsites.net/index.html
 3. **Worker service deployment** - https://mindgeek-mc-worker.azurewebsites.net


### Local - Backend ###
 1. Clone the repository:  `git clone https://DutulescuRemus@bitbucket.org/DutulescuRemus/mindgeek.moviecollection.git`
 2. Open and build the solution **MindGeek.MovieCollection.sln**
 3. For both `MindGeek.MovieCollection.Api` and `MindGeek.MovieCollection.FeedWorkerService`:
    * Set them as startup projects
    * Right click on each project -> Manage User Secrets... The user secrets JSON format is:

```
    {
        "ConnectionStrings:AzureImageCachingBlobStorageConnectionString": "...",
        "ConnectionStrings:AzureLoggingBlobStorageConnectionString": "...",
        "ConnectionStrings:RedisCacheConnectionString": "...",
        "ApplicationSettings:ImageStorageAccountName": "...",
        "ApplicationSettings:ImageStorageAccountKey": "..."
    }
```
    **NOTE**: For security purposes, the user secrets will be provided via email, in a password-protected text file. 
 4. Run the solution (make sure Kestrel is used, not IIS Express!)
 5. (optional) Run integration tests (**<'PATH-TO-REPO'>/test/integration**) collection with Postman
 6. (optional) Use the Swagger UI to make a request to the backend

### Local - Client ###
1. Clone the repository: `git clone https://DutulescuRemus@bitbucket.org/DutulescuRemus/mindgeek.moviecollection.git`
2. Run `ng build && ng serve` on **<'PATH-TO-REPO'>/client/mindgeek-web**
3. Navigate to `http://localhost:4200` to access the client interface

_._
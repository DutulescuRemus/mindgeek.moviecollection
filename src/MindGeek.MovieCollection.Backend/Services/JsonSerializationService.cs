﻿using MindGeek.MovieCollection.Backend.Extensions;
using MindGeek.MovieCollection.Backend.Interfaces;
using Newtonsoft.Json;

namespace MindGeek.MovieCollection.Backend.Services
{
    /// <summary>
    /// Adapter over <see cref="Newtonsoft.Json"/> library
    /// </summary>
    public class JsonSerializationService : IHandleSerialization
    {
        private static readonly JsonSerializerSettings JsonSerializerSettings;

        static JsonSerializationService() =>
            JsonSerializerSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

        /// <summary> Serialize the object </summary>
        /// <typeparam name="T"> Object type </typeparam>
        /// <param name="objectToSerialize"> Object to serialize </param>
        /// <returns> Serialized object (as string) </returns>
        public string SerializeObject<T>(T objectToSerialize) =>
            JsonConvert.SerializeObject(objectToSerialize, Formatting.None, JsonSerializerSettings);

        /// <summary> Deserialize the object after sanitization </summary>
        /// <typeparam name="T"> Type to deserialize by </typeparam>
        /// <param name="serializedObject"> Serialize object to apply deserialization for </param>
        /// <returns> Deserialized object </returns>
        public T DeserializeObject<T>(string serializedObject) =>
            string.IsNullOrEmpty(serializedObject)
                ? default
                : JsonConvert.DeserializeObject<T>(serializedObject.Sanitize(), JsonSerializerSettings);
    }
}

﻿using MindGeek.MovieCollection.Backend.Interfaces;
using MindGeek.MovieCollection.Backend.Services;
using MindGeek.MovieCollection.Data.Feed;
using Xunit;

namespace MingGeek.MovieCollection.UnitTests.Backend.Services
{
    public class JsonSerializationServiceTests
    {
        private readonly IHandleSerialization _systemUnderTest;

        private const string EmptySerialization = "{}";
        private const string MovieWithPropSerialization = "{\"Year\":\"2020\"}";

        public JsonSerializationServiceTests()
            => _systemUnderTest = new JsonSerializationService();


        [Fact]
        public void SerializeNullObject()
        {
            // Arrange
            var nullObjectSerialization = _systemUnderTest
                .SerializeObject<object>(null);

            // Assert
            Assert.Equal("null", nullObjectSerialization);
        }

        [Fact]
        public void SerializeMovieWithProperties()
        {
            // Arrange
            var dummyMovieSerialization = _systemUnderTest
                .SerializeObject(new Movie { Year = "2020" });

            // Assert
            Assert.NotNull(dummyMovieSerialization);
            Assert.Equal(MovieWithPropSerialization, dummyMovieSerialization);
        }

        [Fact]
        public void SerializeEmptyMovieEntity()
        {
            // Arrange
            var dummyMovieSerialization = _systemUnderTest.SerializeObject(new Movie());

            // Assert
            Assert.NotNull(dummyMovieSerialization);
            Assert.Equal(EmptySerialization, dummyMovieSerialization);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(MovieWithPropSerialization)]

        public void DeserializeStringOverMovieEntity(string serializedMovieString)
        {
            // Arrange
            var deserializedMovie = _systemUnderTest.DeserializeObject<Movie>(serializedMovieString);

            // Assert
            switch (serializedMovieString)
            {
                case null:
                    Assert.Null(deserializedMovie);
                    break;
                case "":
                    Assert.Null(deserializedMovie);
                    break;
                case EmptySerialization:
                    Assert.NotNull(deserializedMovie);
                    Assert.Null(deserializedMovie.Year);
                    break;
                case MovieWithPropSerialization:
                    Assert.NotNull(deserializedMovie);
                    Assert.NotNull(deserializedMovie.Year);
                    break;
            }
        }
    }
}

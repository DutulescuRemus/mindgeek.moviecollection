﻿namespace MindGeek.MovieCollection.Data.Feed
{
    /// <summary>
    /// Video class
    /// </summary>
    public class Video
    {
        /// <summary>
        /// Video title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Video URL
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Video type (e.g.: trailer, clip, etc)
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Video alternatives of different quality
        /// </summary>
        public VideoAlternative[] Alternatives { get; set; }
    }
}

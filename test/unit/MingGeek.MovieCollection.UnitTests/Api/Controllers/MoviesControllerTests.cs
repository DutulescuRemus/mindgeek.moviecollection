﻿using Microsoft.AspNetCore.Mvc;
using MindGeek.MovieCollection.Api.Controllers;
using MindGeek.MovieCollection.Backend.Interfaces;
using MindGeek.MovieCollection.Data.Feed;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace MingGeek.MovieCollection.UnitTests.Api.Controllers
{
    public class MoviesControllerTests
    {
        private readonly MoviesController _systemUnderTest;
        private readonly Mock<IServeMovies> _movieServiceMock;

        public MoviesControllerTests()
        {
            _movieServiceMock = new Mock<IServeMovies>();
            _systemUnderTest = new MoviesController(_movieServiceMock.Object);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(10)]
        [InlineData(100)]
        public async Task GetMoviesAsync(int? nofMovieElements)
        {
            // Arrange
            var dummyMovieCollection = MovieCollectionBuilder(nofMovieElements);
            _movieServiceMock.Setup(service => service.GetAllAsync()).ReturnsAsync(dummyMovieCollection);

            // Act
            var getMoviesResponse = await _systemUnderTest.GetMoviesAsync();

            // Assert
            _movieServiceMock.Verify(service => service.GetAllAsync());
            if (dummyMovieCollection == null || dummyMovieCollection.Count == 0)
                Assert.IsType<NotFoundResult>(getMoviesResponse);
            else
            {
                Assert.IsType<OkObjectResult>(getMoviesResponse);
                Assert.Equal(
                    dummyMovieCollection.Count,
                    ((IList<Movie>)((OkObjectResult)getMoviesResponse).Value).Count);
            }
        }

        public static IList<Movie> MovieCollectionBuilder(int? nofMovieElements)
        {
            if (!nofMovieElements.HasValue)
                return null;
            if (nofMovieElements <= 0)
                return Array.Empty<Movie>();

            var movieList = new List<Movie>();

            for (var i = 0; i < nofMovieElements.Value; i++)
                movieList.Add(new Movie());

            return movieList;
        }
    }
}

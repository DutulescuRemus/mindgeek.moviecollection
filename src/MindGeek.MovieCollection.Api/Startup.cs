using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using MindGeek.MovieCollection.Backend.Extensions;
using MindGeek.MovieCollection.Data;

namespace MindGeek.MovieCollection.Api
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        private readonly string _corsPolicyName = "_allowAllOrigins";

        public Startup(IConfiguration configuration)
            => Configuration = configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Application settings model binding
            var appSettings = new ApplicationSettings();
            Configuration.Bind("ApplicationSettings", appSettings);
            services.AddSingleton(appSettings);

            // CORS policy configuration
            ConfigureCors(services);

            // Register custom services
            services.AddCustomServices();

            // Register RedisCache
            services.AddStackExchangeRedisCache(options =>
                options.Configuration = Configuration.GetConnectionString("RedisCacheConnectionString"));

            // Controllers middleware
            services
                .AddControllers()
                .AddJsonOptions(options => options.JsonSerializerOptions.IgnoreNullValues = true);

            // Register Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "MindGeek.MovieCollection API",
                    Description = "Interview project for MindGeek Bucharest",
                    Version = "v1"
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MindGeek.MovieCollection API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseCors(_corsPolicyName);

            app.UseEndpoints(endpoints =>
                endpoints.MapControllers()
                    .RequireCors(_corsPolicyName));
        }

        private void ConfigureCors(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(_corsPolicyName, builder => builder
                    .WithOrigins(
                        "http://localhost:4200",
                        "https://mindgeek-mc-client.herokuapp.com")
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.Backend.Interfaces
{
    public interface ICacheObjects
    {
        Task CacheEntryAsync<T>(string cacheKey, T entry, TimeSpan timeToLive);
        Task<string> GetCachedEntryAsStringAsync(string cacheKey);
        Task<T> GetCachedEntryAsObjectAsync<T>(string cacheKey);
    }
}

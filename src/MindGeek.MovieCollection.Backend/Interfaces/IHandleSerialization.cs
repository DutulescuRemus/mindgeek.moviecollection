﻿namespace MindGeek.MovieCollection.Backend.Interfaces
{
    public interface IHandleSerialization
    {
        string SerializeObject<T>(T objectToSerialize);
        T DeserializeObject<T>(string serializedObject);
    }
}

﻿namespace MindGeek.MovieCollection.Backend.Extensions
{
    public static class StringExtensions
    {
        public static string Sanitize(this string source) =>
            !string.IsNullOrEmpty(source)
                ? source.Replace('�', '\'')
                : source;
    }
}

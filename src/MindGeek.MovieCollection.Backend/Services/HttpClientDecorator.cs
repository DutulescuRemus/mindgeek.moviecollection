﻿using MindGeek.MovieCollection.Backend.Interfaces;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.Backend.Services
{
    public class HttpClientDecorator : IDecorateHttpClient
    {
        private readonly HttpClient _httpClient;

        public HttpClientDecorator() =>
            _httpClient = new HttpClient();

        public async Task<HttpResponseMessage> GetAsync(string requestUrl) =>
            await _httpClient.GetAsync(requestUrl);

        public async Task<Stream> GetStreamAsync(string requestUrl) =>
            await _httpClient.GetStreamAsync(requestUrl);

        public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request) =>
            await _httpClient.SendAsync(request);
    }
}

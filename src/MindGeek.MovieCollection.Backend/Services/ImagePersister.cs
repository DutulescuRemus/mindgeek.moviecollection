﻿using Azure.Storage;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.Extensions.Logging;
using MindGeek.MovieCollection.Backend.Interfaces;
using MindGeek.MovieCollection.Data;
using MindGeek.MovieCollection.Data.Feed;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.Backend.Services
{
    /// <summary>
    /// Handles image persistence-related operations
    /// </summary>
    public class ImagePersister : IPersistImages
    {
        private readonly string _storageAccountName;
        private readonly string _storageAccountKey;
        private readonly ILogger<ImagePersister> _logger;
        private readonly IDecorateHttpClient _httpClient;

        public ImagePersister(
            ApplicationSettings applicationSettings,
            IDecorateHttpClient httpClient,
            ILogger<ImagePersister> logger)
        {
            _storageAccountName = applicationSettings.ImageStorageAccountName
                                  ?? throw new ArgumentNullException(nameof(applicationSettings.ImageStorageAccountName));
            _storageAccountKey = applicationSettings.ImageStorageAccountKey
                ?? throw new ArgumentNullException(nameof(applicationSettings.ImageStorageAccountKey));
            _logger = logger;
            _httpClient = httpClient;
        }

        /// <summary> Get the image from the source URL and save it in the persistence layer </summary>
        /// <param name="sourceUrl"> URL of the image to be persisted </param>
        /// <param name="type"> Image type </param>
        /// <returns></returns>
        public async Task<string> PersistImageFromUrlAsync(string sourceUrl, ImageType type)
        {
            try
            {
                var fileName = ExtractFilenameFromUrl(sourceUrl);

                await using var imageStream = await _httpClient.GetStreamAsync(sourceUrl);

                var blobUri = new Uri(GetFormattedImageUrl(_storageAccountName, fileName, type));
                var storageCredentials = new StorageSharedKeyCredential(_storageAccountName, _storageAccountKey);

                var blobClient = new BlobClient(blobUri, storageCredentials);
                await blobClient.UploadAsync(imageStream, new BlobHttpHeaders { ContentType = "image/jpeg" });

                return blobUri.AbsoluteUri;
            }
            catch (Exception e)
            {
                _logger.LogError("Error during persisting image from URL {url}. Exception - {e}",
                    sourceUrl, e);
                return default;
            }
        }

        /// <summary> Checks whether a particular image was already stored on the persistence level </summary>
        /// <param name="fileUrl"> Url of the image </param>
        /// <param name="type"> Image type </param>
        /// <returns> Image URL if image exists, null otherwise </returns>
        public async Task<string> TryGetPersistedUrlAsync(string fileUrl, ImageType type)
        {
            string fileName = ExtractFilenameFromUrl(fileUrl);
            string persistedFileUrl = GetFormattedImageUrl(_storageAccountName, fileName, type);

            return await ExistsAsync(persistedFileUrl)
                ? persistedFileUrl
                : default;
        }

        /// <summary> Check if an image exists at the given URL </summary>
        /// <param name="url"> File URL </param>
        /// <returns> Whether the image exists </returns>
        public async Task<bool> ExistsAsync(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Head, url);
            var headResponse = await _httpClient.SendAsync(request);

            return headResponse.StatusCode == HttpStatusCode.OK;
        }

        /// <summary>
        /// Expected url format: {baseAddress}/.../{fileName}
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string ExtractFilenameFromUrl(string url)
        {
            int filenameStartIndex = url.LastIndexOf('/') + 1;
            int fileExtensionIndex = url.LastIndexOf('.') + 1;

            if (fileExtensionIndex < filenameStartIndex)
                throw new Exception($"URL {url} of a supposed file not well formatted.");

            return !string.IsNullOrEmpty(url)
                ? url.Substring(filenameStartIndex)
                : default;
        }

        private string GetFormattedImageUrl(string storageAccountName, string fileName, ImageType imageType)
            => $"https://{storageAccountName}.blob.core.windows.net/{imageType.ToString().ToLowerInvariant()}/{fileName}";
    }
}

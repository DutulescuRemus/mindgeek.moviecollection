﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.Backend.Interfaces
{
    public interface IDecorateHttpClient
    {
        Task<HttpResponseMessage> GetAsync(string requestUrl);
        Task<Stream> GetStreamAsync(string requestUrl);
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request);
    }
}

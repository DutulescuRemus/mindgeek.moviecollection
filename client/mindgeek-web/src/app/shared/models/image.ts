export class Image {
  url: string;
  h: number;
  w: number;
}

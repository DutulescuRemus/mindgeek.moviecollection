﻿namespace MindGeek.MovieCollection.Data.Feed
{
    /// <summary>
    /// Image container
    /// </summary>
    public class Image
    {
        /// <summary>
        /// Image URL
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Image height
        /// </summary>
        public int H { get; set; }

        /// <summary>
        /// Image width
        /// </summary>
        public int W { get; set; }
    }

    public enum ImageType
    {
        Card = 0,
        KeyArt = 1
    }
}

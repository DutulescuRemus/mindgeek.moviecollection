﻿using MindGeek.MovieCollection.Backend.Interfaces;
using MindGeek.MovieCollection.Data;
using MindGeek.MovieCollection.Data.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.Backend.Services
{
    /// <summary>
    /// Retrieves <see cref="Movie"/> entities in API form
    /// </summary>
    public class MovieService : IServeMovies
    {
        private readonly ICacheObjects _cachingService;
        private readonly IProcessFeed _feedProcessor;
        private readonly ApplicationSettings _applicationSettings;

        public MovieService(
            ICacheObjects cachingService,
            IProcessFeed feedProcessor,
            ApplicationSettings applicationSettings)
        {
            _cachingService = cachingService ?? throw new ArgumentNullException(nameof(cachingService));
            _feedProcessor = feedProcessor ?? throw new ArgumentNullException(nameof(feedProcessor));
            _applicationSettings = applicationSettings ?? throw new ArgumentNullException(nameof(applicationSettings));
        }

        /// <summary> Retrieves the movie collection in a form ready for API consumption </summary>
        /// <returns> Movie collection </returns>
        public async Task<IList<Movie>> GetAllAsync()
        {
            var cachedMovies = await _cachingService.GetCachedEntryAsObjectAsync<IList<Movie>>(
                _applicationSettings.MovieCollectionCacheKey);

            if (cachedMovies == null || !cachedMovies.Any())
                cachedMovies = await _feedProcessor.ProcessMovieFeedAsync();

            return cachedMovies ?? Array.Empty<Movie>();
        }

        /// <summary>
        /// Gets movie details
        /// </summary>
        /// <param name="movieId"> Movie unique identifier </param>
        /// <returns></returns>
        public async Task<Movie> GetById(string movieId) =>
            await _cachingService.GetCachedEntryAsObjectAsync<Movie>(movieId);
    }
}

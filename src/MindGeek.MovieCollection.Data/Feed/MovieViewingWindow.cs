﻿namespace MindGeek.MovieCollection.Data.Feed
{
    /// <summary>
    /// Window of availability for streaming
    /// </summary>
    public class MovieViewingWindow
    {
        /// <summary>
        /// Streaming availability start date
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// Streaming availability end date
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Streaming network where the movie is available for streaming
        /// </summary>
        public string WayToWatch { get; set; }
    }
}

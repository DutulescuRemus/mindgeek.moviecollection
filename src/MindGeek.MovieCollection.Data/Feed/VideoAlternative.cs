﻿namespace MindGeek.MovieCollection.Data.Feed
{
    /// <summary>
    /// Video container
    /// </summary>
    public class VideoAlternative
    {
        /// <summary>
        /// Quality of the video
        /// </summary>
        public string Quality { get; set; }

        /// <summary>
        /// Video URL
        /// </summary>
        public string Url { get; set; }
    }
}

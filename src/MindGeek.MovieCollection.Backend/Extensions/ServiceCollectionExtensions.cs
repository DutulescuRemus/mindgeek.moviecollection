﻿using Microsoft.Extensions.DependencyInjection;
using MindGeek.MovieCollection.Backend.Interfaces;
using MindGeek.MovieCollection.Backend.Services;

namespace MindGeek.MovieCollection.Backend.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomServices(this IServiceCollection services)
        {
            services.AddSingleton<IHandleSerialization, JsonSerializationService>();
            services.AddScoped<IDecorateHttpClient, HttpClientDecorator>();
            services.AddScoped<IServeMovies, MovieService>();
            services.AddScoped<ICacheObjects, CachingService>();
            services.AddScoped<IPersistImages, ImagePersister>();
            services.AddScoped<IFeedMovies, MovieFeedService>();
            services.AddScoped<IProcessFeed, FeedProcessor>();

            return services;
        }
    }
}

export const environment = {
  production: true,
  moviesApi: 'https://mindgeek-mc-api.azurewebsites.net/api/movies'
};

import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Movie } from '../models/movie';
import { ErrorHandler } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})

export class MovieService {
  constructor(
    private http: HttpClient,
    private errorHandler: ErrorHandler
  ) { }

  getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(environment.moviesApi)
      .pipe(
        retry(1),
        catchError(this.errorHandler.handleError)
      );
  }

  getById(id: string) {
    return this.http.get<Movie>(`${environment.moviesApi}/${id}`)
      .pipe(
        retry(1),
        catchError(this.errorHandler.handleError)
      );
  }
}

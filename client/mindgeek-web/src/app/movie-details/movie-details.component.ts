import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '../shared/models/movie';
import { MovieService } from '../shared/services/movie.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {
  @Input() movie: Movie;

  public imageUrls: string[];

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(data => {
      this.movieService.getById(data.id).subscribe(d => {
        this.movie = d;
        this.imageUrls = [];
        this.movie.cardImages.forEach(img => {
          this.imageUrls.push(img.url);
        });
      });
    });
  }
}

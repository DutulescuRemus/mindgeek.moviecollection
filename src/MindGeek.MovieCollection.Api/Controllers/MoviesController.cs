﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MindGeek.MovieCollection.Backend.Interfaces;
using MindGeek.MovieCollection.Data.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.Api.Controllers
{
    /// <summary> Movies API </summary>
    [Route("api/movies")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IServeMovies _movieService;

        public MoviesController(IServeMovies movieService) =>
            _movieService = movieService ?? throw new ArgumentNullException(nameof(movieService));

        /// <summary> Retrieves all <see cref="Movie"/> entities </summary>
        /// <returns><see cref="Movie"/> collection</returns>
        [HttpGet]
        [Route("")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(IList<Movie>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetMoviesAsync()
        {
            var movies = await _movieService.GetAllAsync();

            if (movies == null || !movies.Any())
                return NotFound();

            return Ok(movies);
        }

        /// <summary> Retrieves the <see cref="Movie"/> details </summary>
        /// <returns><see cref="Movie"/></returns>
        [HttpGet]
        [Route("{movieId}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(IList<Movie>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetById(string movieId)
        {
            if (string.IsNullOrEmpty(movieId))
                return BadRequest("Malformed movie ID");

            var movie = await _movieService.GetById(movieId);

            if (movie == null)
                return NotFound();

            return Ok(movie);
        }
    }
}
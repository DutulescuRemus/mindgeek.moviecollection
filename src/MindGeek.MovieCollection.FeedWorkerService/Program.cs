using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.AzureKeyVault;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MindGeek.MovieCollection.Backend.Extensions;
using MindGeek.MovieCollection.Data;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace MindGeek.MovieCollection.FeedWorkerService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
                        .AddJsonFile("appSettings.json", false, true)
                        .AddJsonFile($"appSettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", true, true)
                        .AddEnvironmentVariables();

                    if (hostingContext.HostingEnvironment.IsDevelopment())
                        config.AddUserSecrets<MovieFeedWorker>();
                    else
                    {
                        var builtConfig = config.Build();
                        var appSettings = new ApplicationSettings();
                        builtConfig.Bind("ApplicationSettings", appSettings);

                        var azureServiceTokenProvider = new AzureServiceTokenProvider();
                        var keyVaultClient = new KeyVaultClient(
                            new KeyVaultClient.AuthenticationCallback(
                                azureServiceTokenProvider.KeyVaultTokenCallback));
                        config.AddAzureKeyVault(
                            $"https://{appSettings.KeyVaultName}.vault.azure.net/",
                            keyVaultClient,
                            new DefaultKeyVaultSecretManager());
                    }

                })
                .Build()
                .Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog((context, configuration) =>
                {
                    configuration
                        .MinimumLevel.Debug()
                        .MinimumLevel.Override("Microsoft", LogEventLevel.Verbose)
                        .MinimumLevel.Override("System", LogEventLevel.Verbose)
                        .Enrich.FromLogContext()
                        .WriteTo.Console(
                            outputTemplate:
                            "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}",
                            theme: AnsiConsoleTheme.Literate)
                        .WriteTo.AzureBlobStorage(
                            context.Configuration.GetConnectionString("AzureLoggingBlobStorageConnectionString"),
                            LogEventLevel.Information,
                            "worker");
                })
                .ConfigureServices((hostContext, services) =>
                {
                    // Application settings model binding
                    var appSettings = new ApplicationSettings();
                    hostContext.Configuration.Bind("ApplicationSettings", appSettings);
                    services.AddSingleton(appSettings);

                    // Register custom services
                    services.AddCustomServices();

                    // Register RedisCache
                    services.AddStackExchangeRedisCache(options =>
                        options.Configuration = hostContext.Configuration.GetConnectionString("RedisCacheConnectionString"));

                    // Register worker service
                    services.AddHostedService<MovieFeedWorker>();
                });
    }
}

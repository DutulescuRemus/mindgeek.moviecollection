﻿using Microsoft.Extensions.Logging;
using MindGeek.MovieCollection.Backend.Interfaces;
using MindGeek.MovieCollection.Data;
using MindGeek.MovieCollection.Data.Feed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.Backend.Services
{
    /// <summary>
    /// Facade for the entire feed processing logic
    /// </summary>
    public class FeedProcessor : IProcessFeed
    {
        private readonly IFeedMovies _movieFeedService;
        private readonly ICacheObjects _cachingService;
        private readonly IPersistImages _imagePersister;
        private readonly TimeSpan _cacheTimeToLive;
        private readonly string _movieCacheKey;
        private readonly ILogger<FeedProcessor> _logger;

        public FeedProcessor(
            IFeedMovies movieFeedService,
            ICacheObjects cachingService,
            IPersistImages imagePersister,
            ApplicationSettings applicationSettings,
            ILogger<FeedProcessor> logger)
        {
            _movieFeedService = movieFeedService;
            _cachingService = cachingService;
            _imagePersister = imagePersister;
            _cacheTimeToLive = TimeSpan.FromSeconds(applicationSettings.CacheTimeToLive);
            _movieCacheKey = applicationSettings.MovieCollectionCacheKey;
            _logger = logger;
        }

        /// <summary> Process the feed data & cache result </summary>
        /// <returns> Movie entities collection </returns>
        public async Task<IList<Movie>> ProcessMovieFeedAsync()
        {
            // 1. Retrieve latest feed data
            var feedData = await _movieFeedService.GetLatestFeedAsync();
            if (feedData != null && feedData.Any())
            {
                // 2. Process each movie entry
                foreach (var movie in feedData)
                    await ProcessMovieFeedEntityAsync(movie);

                // 3. Set cache
                await _cachingService.CacheEntryAsync(_movieCacheKey, feedData, _cacheTimeToLive);
            }

            return feedData;
        }

        private async Task ProcessMovieFeedEntityAsync(Movie movie)
        {
            await ProcessImageCollectionAsync(movie.CardImages, ImageType.Card);
            await ProcessImageCollectionAsync(movie.KeyArtImages, ImageType.KeyArt);

            await _cachingService.CacheEntryAsync(movie.Id, movie, _cacheTimeToLive);
        }

        private async Task ProcessImageCollectionAsync(IList<Image> images, ImageType type)
        {
            // iterated in reverse to allow for elements removal during iteration
            for (int i = images.Count - 1; i >= 0; i--)
            {
                try
                {
                    // 1. Check if image persisted at a previous time
                    var imageUrl = await _imagePersister.TryGetPersistedUrlAsync(images[i].Url, type);
                    if (imageUrl == default)
                    {
                        // 2. Check if feed URL is valid
                        if (!await _imagePersister.ExistsAsync(images[i].Url))
                        {
                            // If no image at feed URL => remove Image entity
                            images.RemoveAt(i);
                            continue;
                        }

                        // 3. Persist image
                        var persistedImageUrl = await _imagePersister.PersistImageFromUrlAsync(
                            images[i].Url,
                            type);
                        if (!string.IsNullOrEmpty(persistedImageUrl))
                            images[i].Url = persistedImageUrl;
                        else
                            _logger.LogWarning(
                                "Image {image} not successfully persisted. Feed url will be unchanged!",
                                images[i]);
                    }
                    else
                    {
                        // If already persisted => use URL
                        images[i].Url = imageUrl;
                    }
                }
                catch (Exception e)
                {
                    // Something went wrong => remove image from collection
                    _logger.LogError("Exception during processing of {image}. Exception - {e}", images[i], e);
                    images.RemoveAt(i);
                }
            }
        }
    }
}

﻿using Microsoft.Extensions.Logging;
using MindGeek.MovieCollection.Backend.Interfaces;
using MindGeek.MovieCollection.Data;
using MindGeek.MovieCollection.Data.Feed;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.Backend.Services
{
    /// <summary>
    /// Gets the feed data and deserializes it to <see cref="Movie"/> model collection
    /// </summary>
    public class MovieFeedService : IFeedMovies
    {
        private readonly IHandleSerialization _jsonSerializationService;
        private readonly string _movieFeedUrl;
        private readonly IDecorateHttpClient _httpClient;
        private readonly ILogger<MovieFeedService> _logger;

        public MovieFeedService(
            IHandleSerialization jsonSerializationService,
            IDecorateHttpClient httpClient,
            ILogger<MovieFeedService> logger,
            ApplicationSettings applicationSettings)
        {
            _jsonSerializationService = jsonSerializationService
                ?? throw new ArgumentNullException(nameof(jsonSerializationService));
            _logger = logger
                ?? throw new ArgumentNullException(nameof(logger));
            _movieFeedUrl = applicationSettings.FeedUrl
                ?? throw new ArgumentNullException(nameof(applicationSettings));
            _httpClient = httpClient;
        }

        /// <summary> Retrieves the movie collection from the feed, without any processing </summary>
        /// <returns> Movie collection (untouched) </returns>
        public async Task<IList<Movie>> GetLatestFeedAsync()
        {
            var getFeedResponse = await _httpClient.GetAsync(_movieFeedUrl);

            if (!getFeedResponse.IsSuccessStatusCode)
            {
                _logger.LogWarning(
                    $"[{nameof(GetLatestFeedAsync)}] GET request to feed URL {_movieFeedUrl} returned " +
                    $"unsuccessful status code {getFeedResponse.StatusCode}");
                return default;
            }

            var serializedData = await getFeedResponse.Content.ReadAsStringAsync();

            return _jsonSerializationService.DeserializeObject<Movie[]>(serializedData);
        }
    }
}

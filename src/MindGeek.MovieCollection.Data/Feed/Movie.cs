﻿using System;
using System.Collections.Generic;

namespace MindGeek.MovieCollection.Data.Feed
{
    /// <summary>
    /// Movie container
    /// </summary>
    public class Movie
    {
        /// <summary>
        /// Movie unique identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Movie synopsis
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Movie images in card form
        /// </summary>
        public IList<Image> CardImages { get; set; }

        /// <summary>
        /// Original cast of the movie
        /// </summary>
        public Person[] Cast { get; set; }

        /// <summary>
        /// Movie certification
        /// </summary>
        public string Cert { get; set; }

        /// <summary>
        /// Media type
        /// </summary>
        public string Class { get; set; }

        /// <summary>
        /// Movie directors
        /// </summary>
        public Person[] Directors { get; set; }

        /// <summary> 
        /// Duration in seconds 
        /// </summary>
        public int? Duration { get; set; }

        /// <summary>
        /// Movie genres
        /// </summary>
        public string[] Genres { get; set; }

        /// <summary>
        /// Short phrase describing the movie
        /// </summary>
        public string Headline { get; set; }

        /// <summary>
        /// Key artistic images, representative of the movie
        /// </summary>
        public IList<Image> KeyArtImages { get; set; }

        /// <summary>
        /// Date when the feed's movie entry was last updated.
        /// Expected format: YYYY-MM-DD
        /// </summary>
        public DateTime? LastUpdated { get; set; }

        /// <summary>
        /// A short quote representative for the movie
        /// </summary>
        public string Quote { get; set; }

        /// <summary>
        /// <see cref="ReviewAuthor"/>'s rating of the movie
        /// </summary>
        public int? Rating { get; set; }

        /// <summary>
        /// Author of the movie review
        /// </summary>
        public string ReviewAuthor { get; set; }

        /// <summary>
        /// SkyGo streaming client unique identifier
        /// </summary>
        public string SkyGoId { get; set; }

        /// <summary>
        /// SkyGo streaming address of the movie
        /// </summary>
        public string SkyGoUrl { get; set; }

        /// <summary>
        /// Checksum used for data integrity verification
        /// </summary>
        public string Sum { get; set; }

        /// <summary>
        /// Review URL
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Clips and trailers of the movie
        /// </summary>
        public IList<Video> Videos { get; set; }

        /// <summary>
        /// Window of availability for streaming
        /// </summary>
        public MovieViewingWindow ViewingWindow { get; set; }

        /// <summary>
        /// Year of release
        /// </summary>
        public string Year { get; set; }
    }
}

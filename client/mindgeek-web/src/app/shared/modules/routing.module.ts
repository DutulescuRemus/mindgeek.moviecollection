import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieListComponent } from 'src/app/movie-list/movie-list.component';
import { MovieDetailsComponent } from 'src/app/movie-details/movie-details.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: MovieListComponent },
  { path: 'details/:id', component: MovieDetailsComponent,  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class RoutingModule { }

export const routingComponents = [
  MovieListComponent,
  MovieDetailsComponent
];

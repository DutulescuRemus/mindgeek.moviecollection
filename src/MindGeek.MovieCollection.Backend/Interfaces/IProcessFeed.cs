﻿using MindGeek.MovieCollection.Data.Feed;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.Backend.Interfaces
{
    public interface IProcessFeed
    {
        Task<IList<Movie>> ProcessMovieFeedAsync();
    }
}

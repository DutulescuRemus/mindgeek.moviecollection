import { Component, OnInit } from '@angular/core';
import { MovieService } from '../shared/services/movie.service';
import { Movie } from '../shared/models/movie';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {

  movies: Movie[];

  constructor(
    private movieService: MovieService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.populateMovieData();
  }

  populateMovieData() {
    this.movieService.getMovies().subscribe(data => {
      this.movies = data;
      console.log(data);
    });
  }

  goToMovieDetails(movieId: string) {
    this.router.navigate(['/details', movieId]);
  }
}

﻿namespace MindGeek.MovieCollection.Data
{
    public class ApplicationSettings
    {
        public string FeedUrl { get; set; }
        public string MovieCollectionCacheKey { get; set; }
        public int CacheTimeToLive { get; set; }
        public string ImageStorageAccountName { get; set; }
        public string ImageStorageAccountKey { get; set; }
        public string KeyVaultName { get; set; }
    }
}

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MindGeek.MovieCollection.Backend.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.FeedWorkerService
{
    public class MovieFeedWorker : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly ILogger<MovieFeedWorker> _logger;

        public MovieFeedWorker(
            IServiceScopeFactory serviceScopeFactory,
            ILogger<MovieFeedWorker> logger)
        {
            _serviceScopeFactory = serviceScopeFactory ?? throw new ArgumentNullException(nameof(serviceScopeFactory));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation($"{nameof(MovieFeedWorker)} is STARTING");

            await ProcessFeedAsync();

            _logger.LogInformation($"{nameof(MovieFeedWorker)} FINISHED");
        }

        private async Task ProcessFeedAsync()
        {
            _logger.LogInformation($"[{nameof(ProcessFeedAsync)}]: starting to process feed...");

            try
            {
                var scope = _serviceScopeFactory.CreateScope();
                var feedProcessor = scope.ServiceProvider
                    .GetRequiredService<IProcessFeed>();

                await feedProcessor.ProcessMovieFeedAsync();

                _logger.LogInformation($"[{nameof(ProcessFeedAsync)}]: ...finished processing feed!");
            }
            catch (Exception e)
            {
                _logger.LogError("Exception during feed processing execution: {e}", e);
            }
        }
    }
}

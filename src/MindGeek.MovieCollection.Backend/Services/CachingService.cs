﻿using Microsoft.Extensions.Caching.Distributed;
using MindGeek.MovieCollection.Backend.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.Backend.Services
{
    /// <summary>
    /// Adapter pattern over <see cref="IDistributedCache"/> for caching objects
    /// </summary>
    public class CachingService : ICacheObjects
    {
        private readonly IDistributedCache _distributedCache;
        private readonly IHandleSerialization _jsonSerializationService;

        public CachingService(
            IDistributedCache distributedCache,
            IHandleSerialization jsonSerializationService)
        {
            _distributedCache = distributedCache
                ?? throw new ArgumentNullException(nameof(distributedCache));
            _jsonSerializationService = jsonSerializationService
                ?? throw new ArgumentNullException(nameof(jsonSerializationService));
        }

        /// <summary> Set cache entry </summary>
        /// <typeparam name="T"> cached object type </typeparam>
        /// <param name="cacheKey"> Key to set the cache at </param>
        /// <param name="entry"> Object to be cached</param>
        /// <param name="timeToLive"> Cache expiration time </param>
        /// <returns></returns>
        public async Task CacheEntryAsync<T>(string cacheKey, T entry, TimeSpan timeToLive)
        {
            if (entry != null)
            {
                var serializedObj = _jsonSerializationService.SerializeObject(entry);
                await _distributedCache.SetStringAsync(cacheKey, serializedObj, new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = timeToLive
                });
            }
        }

        /// <summary> Retrieve cached value at the given key, as POCO collection </summary>
        /// <typeparam name="T"> Cached object type </typeparam>
        /// <param name="cacheKey"> Key to get the cache for </param>
        /// <returns> Cached entries, deserialized as collection </returns>
        public async Task<T> GetCachedEntryAsObjectAsync<T>(string cacheKey)
        {
            var cachedData = await GetCachedEntryAsStringAsync(cacheKey);

            if (string.IsNullOrEmpty(cachedData))
                return default;

            return _jsonSerializationService.DeserializeObject<T>(cachedData)
                   ?? default;
        }

        /// <summary> Retrieve cached value at the given key </summary>
        /// <param name="cacheKey"> Key to get the cache for </param>
        /// <returns> Cached entry (serialized) as string </returns>
        public async Task<string> GetCachedEntryAsStringAsync(string cacheKey)
            => await _distributedCache.GetStringAsync(cacheKey);
    }
}

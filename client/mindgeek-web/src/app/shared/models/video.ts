import { VideoAlternative } from './video-alternative';

export class Video {
  title: string;
  url: string;
  type: string;
  altenatives: VideoAlternative[];
}

﻿using MindGeek.MovieCollection.Data.Feed;
using System.Threading.Tasks;

namespace MindGeek.MovieCollection.Backend.Interfaces
{
    public interface IPersistImages
    {
        Task<string> TryGetPersistedUrlAsync(string fileUrl, ImageType type);
        Task<string> PersistImageFromUrlAsync(string sourceUrl, ImageType type);
        Task<bool> ExistsAsync(string url);
    }
}
